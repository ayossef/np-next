import {useRouter} from 'next/router'
import PersonalInfo from "../../components/PersonalInfo"

const Profile = () => { 
    const router = useRouter()
    const {username} = router.query 
    return (<div>
         <h1>Profile Page</h1>
    <PersonalInfo name={username}></PersonalInfo>
    </div>)
 }

 export default Profile