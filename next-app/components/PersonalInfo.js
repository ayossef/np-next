function PersonalInfo(props) {
    return (
        <div>
            <img width={150} src='https://as1.ftcdn.net/v2/jpg/05/40/28/44/1000_F_540284423_A9g0ZYucKq0qRfzhknsjVeBtK037aOvM.jpg'></img>
            <p>{props.name}</p>
            <p>ahmed.yossef@nobleprog.com</p>
        </div>
    )
}

export default PersonalInfo