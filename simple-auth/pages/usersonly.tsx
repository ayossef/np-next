import { NextPage } from "next";
import { useSession } from "next-auth/react";
import Router from "next/router";
import { useEffect } from "react";

const Usersonly: NextPage = (): JSX.Element => {
  const { status, data } = useSession();

  useEffect(() => {
    if (status === "unauthenticated") Router.replace("/auth/signin");
  }, [status]);

  if (status === "authenticated")
    return (
      <div>
        This page is Protected for special people. like{"\n"}
        {JSON.stringify(data.user, null, 2)}
        <button onClick={()=>{Router.replace('http://localhost:3000/api/auth/signout')}}>Logout</button>
      </div>
    );

  return <div>loading
  </div>;
};

export default Usersonly;
